package ru.admhm.helpdesk.domain.task.repository

import io.reactivex.Single
import ru.admhm.helpdesk.domain.task.model.TaskModel
import ru.admhm.helpdesk.domain.task.model.TaskUserModel

interface TaskRepository {

    fun getTask(taskId: Long): Single<TaskModel>

    fun getTaskUsers(taskId: Long): Single<List<TaskUserModel>>
}