package ru.admhm.helpdesk.domain.auth.repository

import io.reactivex.Completable
import ru.admhm.helpdesk.domain.auth.model.SignInModel

interface AuthRepository {

    fun signIn(signIn: SignInModel): Completable

    fun isAuthorized(): Completable
}