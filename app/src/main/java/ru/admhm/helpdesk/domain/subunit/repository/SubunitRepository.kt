package ru.admhm.helpdesk.domain.subunit.repository

import io.reactivex.Single
import ru.admhm.helpdesk.domain.subunit.model.SubunitModel

interface SubunitRepository {

    fun getSubunitByTask(taskId: Long): Single<SubunitModel>
}