package ru.admhm.helpdesk.domain.task.model

data class PriorityModel(
    val id: Int,
    val name: String = when (id) {
        1 -> "Очень низкий"
        2 -> "Низкий"
        3 -> "Средний"
        4 -> "Высокий"
        5 -> "Очень высокий"
        else -> "Unknown"
    }
)