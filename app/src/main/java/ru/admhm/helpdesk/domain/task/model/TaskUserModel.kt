package ru.admhm.helpdesk.domain.task.model

import com.google.gson.annotations.SerializedName
import ru.admhm.helpdesk.domain.user.model.UserModel

data class TaskUserModel(
    @SerializedName("id")
    val id: Long = 0,

    @SerializedName("users_id")
    val userId: Long = 0,

    @SerializedName("type")
    val type: Int = 0,

    val user: UserModel? = null
) {

    companion object {
        const val TYPE_AUTHOR = 1
        const val TYPE_ASSIGNED = 2
        const val TYPE_OBSERVER = 3
    }
}