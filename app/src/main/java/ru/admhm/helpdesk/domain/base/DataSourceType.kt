package ru.admhm.helpdesk.domain.base

enum class DataSourceType {
    REMOTE, CACHE
}