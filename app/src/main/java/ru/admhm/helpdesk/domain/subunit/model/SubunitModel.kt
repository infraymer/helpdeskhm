package ru.admhm.helpdesk.domain.subunit.model

import com.google.gson.annotations.SerializedName

data class SubunitModel(
    @SerializedName("id")
    val id: Long = 0,
    @SerializedName("name")
    val name: String = ""
)