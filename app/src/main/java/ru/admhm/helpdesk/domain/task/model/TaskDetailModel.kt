package ru.admhm.helpdesk.domain.task.model

import ru.admhm.helpdesk.domain.subunit.model.SubunitModel
import ru.admhm.helpdesk.domain.user.model.UserModel

data class TaskDetailModel(
    val taskModel: TaskModel,
    val initiatorUser: UserModel?,
    val performerUser: UserModel?,
    val subunit: SubunitModel?
)