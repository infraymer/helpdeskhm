package ru.admhm.helpdesk.domain.user.model

data class UserModel(
    val id: Long = 0,
    val name: String = "",
    val lastName: String = "",
    val firstName: String = ""
)