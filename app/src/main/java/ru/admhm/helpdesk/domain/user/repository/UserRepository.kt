package ru.admhm.helpdesk.domain.user.repository

import io.reactivex.Single
import ru.admhm.helpdesk.domain.user.model.UserModel

interface UserRepository {

    fun getUser(userId: Long): Single<UserModel>
}