package ru.admhm.helpdesk.domain.auth.interactor

import io.reactivex.Completable
import ru.admhm.helpdesk.domain.auth.model.SignInModel
import ru.admhm.helpdesk.domain.auth.repository.AuthRepository

class AuthInteractor(
    private val authRepository: AuthRepository
) {

    fun signIn(singIn: SignInModel): Completable =
        authRepository.signIn(singIn)

    fun isAuthorized(): Completable =
        authRepository.isAuthorized()
}