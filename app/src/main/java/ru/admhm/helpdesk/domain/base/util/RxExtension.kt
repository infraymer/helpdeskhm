package ru.admhm.helpdesk.domain.base.util

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

inline fun <T> Maybe<T>.doCompletableOnSuccess(crossinline completableSource: (T) -> Completable): Maybe<T> =
    flatMap { completableSource(it).andThen(Maybe.just(it)) }

inline fun <T> Single<T>.doCompletableOnSuccess(crossinline completableSource: (T) -> Completable): Single<T> =
    flatMap { completableSource(it).andThen(Single.just(it)) }

fun <T> Iterable<T>.toMaybe(): Maybe<T> =
    firstOrNull()?.let { Maybe.just(it) } ?: Maybe.empty()

fun <T> Single<List<T>>.maybeFirst(): Maybe<T> =
    flatMapMaybe { it.toMaybe() }

/**
 * Empty on error stub in order to suppress errors
 */
val emptyOnErrorStub: (Throwable) -> Unit = {}