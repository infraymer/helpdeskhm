package ru.admhm.helpdesk.domain.task.model

import java.util.*

data class TaskModel(
    val id: Long,
    val name: String,
    val content: String,
    val priority: PriorityModel,
    val status: StatusModel,
    val initiatorId: Long,
    val subunitsId: Long,
    val solveDate: Date,
    val closeDate: Date,
    val creationDate: Date
)