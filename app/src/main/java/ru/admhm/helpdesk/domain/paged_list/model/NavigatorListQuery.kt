package ru.admhm.helpdesk.domain.paged_list.model

class NavigatorListQuery(
    val page: Int = 1,
    val pageSize: Int = 20,

    val authorId: Long? = null
)