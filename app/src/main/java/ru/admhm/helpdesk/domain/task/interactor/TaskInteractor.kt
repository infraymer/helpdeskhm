package ru.admhm.helpdesk.domain.task.interactor

import io.reactivex.Single
import io.reactivex.rxkotlin.Singles.zip
import io.reactivex.rxkotlin.toObservable
import ru.admhm.helpdesk.domain.subunit.model.SubunitModel
import ru.admhm.helpdesk.domain.subunit.repository.SubunitRepository
import ru.admhm.helpdesk.domain.task.model.TaskDetailModel
import ru.admhm.helpdesk.domain.task.model.TaskUserModel
import ru.admhm.helpdesk.domain.task.repository.TaskRepository
import ru.admhm.helpdesk.domain.user.model.UserModel
import ru.admhm.helpdesk.domain.user.repository.UserRepository

class TaskInteractor(
    private val taskRepository: TaskRepository,
    private val userRepository: UserRepository,
    private val subunitRepository: SubunitRepository
) {

    fun getTaskDetail(taskId: Long): Single<TaskDetailModel> =
        zip(
            taskRepository
                .getTask(taskId),
            getTaskUsers(taskId),
            subunitRepository
                .getSubunitByTask(taskId)
                .onErrorReturnItem(SubunitModel())
            /*.map { it.sortedBy { it.type } }
            .flatMapObservable { it.toObservable() }
            .flatMapSingle { userRepository.getUser(it.userId) }
            .toList()*/
        ) { task, users, subunit ->
            val author = users.find { it.type == TaskUserModel.TYPE_AUTHOR }
            val assigned = users.find { it.type == TaskUserModel.TYPE_ASSIGNED }
            TaskDetailModel(task, author?.user, assigned?.user, subunit.takeIf { it.id != 0L })
        }


    private fun getTaskUsers(taskId: Long) =
        taskRepository.getTaskUsers(taskId)
            /*.map {
                val author = it.find { it.type == TaskUserModel.TYPE_AUTHOR } ?: TaskUserModel()
                val assigned = it.find { it.type == TaskUserModel.TYPE_ASSIGNED } ?: TaskUserModel()
                listOf(author, assigned)
            }*/
            .flatMapObservable { it.toObservable() }
            .flatMapSingle { taskUser ->
                userRepository
                    .getUser(taskUser.userId)
                    .onErrorReturnItem(UserModel())
                    .map { taskUser.copy(user = it.takeIf { it.id != 0L }) }
            }
            .toList()

}