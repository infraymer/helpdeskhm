package ru.admhm.helpdesk.domain.task_list.repository

import io.reactivex.Single
import ru.admhm.helpdesk.domain.base.model.PagedList
import ru.admhm.helpdesk.domain.paged_list.model.NavigatorListQuery
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel

interface TaskListRepository {

    fun getTaskListItems(query: NavigatorListQuery): Single<PagedList<TaskListItemModel>>
}