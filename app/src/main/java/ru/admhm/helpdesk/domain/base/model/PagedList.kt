package ru.admhm.helpdesk.domain.base.model

class PagedList<E>(
    list: List<E>,

    val page: Int = 1,
    val hasNext: Boolean = false,
    val hasPrev: Boolean = false,
    val total: Int = 0
) : List<E> by list

fun <E> List<E>.concatWithPagedList(pagedList: PagedList<E>): PagedList<E> =
    PagedList(this + pagedList, page = pagedList.page, hasNext = pagedList.hasNext, hasPrev = pagedList.hasPrev, total = pagedList.total)