package ru.admhm.helpdesk.domain.auth.model

data class SignInModel(
    val login: String,
    val password: String
)