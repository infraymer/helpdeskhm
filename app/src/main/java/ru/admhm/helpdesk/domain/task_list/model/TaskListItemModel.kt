package ru.admhm.helpdesk.domain.task_list.model

import ru.admhm.helpdesk.domain.task.model.PriorityModel
import ru.admhm.helpdesk.domain.task.model.StatusModel
import java.util.*

data class TaskListItemModel(
    val id: Long,
    val name: String,
    val priority: PriorityModel,
    val timeOpen: Date,
    val specialistId: Long,
    val statusId: StatusModel
)