package ru.admhm.helpdesk.domain.task.model

data class StatusModel(
    val id: Int,
    val name: String = when (id) {
        1 -> "Новая"
        2 -> "В работе (назначена)"
        3 -> "В работе (запланирована)"
        4 -> "Ожидающие решения"
        5 -> "Решена"
        6 -> "Закрыта"
        else -> "Unknown"
    }
)