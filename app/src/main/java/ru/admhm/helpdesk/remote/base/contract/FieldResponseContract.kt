package ru.admhm.helpdesk.remote.base.contract

class FieldResponseContract {
    companion object {
        // ID - "2": 24327,
        const val ID = "2"
        // Наименование
        const val NAME = "1"
        // Статус - "12": 6,
        const val STATUS = "12"
        // Приоритет - "3": 3,
        const val PRIORITY = "3"
        // Заголовок - "1": " каб 409. ул. Карла Маркса, 14, Настроить новую ЭЦП",
        const val DESCRIPTION = "21"
        // Описание - "21": "....",
        const val TIME_OPEN = "15"
        // Дата открытия - "15": "2019-04-25 15:35:03",
        const val TIME_LAST_CHANGE = "19"
        // Последнее изменение - "19": "2019-04-28 09:59:46",
        const val TIME_TO_RESOLVE = "18"
        // Время до решения - "18": "2019-04-30 16:05:03",
        const val TIME_TO_RESOLVE_WITH_PROGRESS = "151"
        // Время до решения + Прогресса - "151": "2019-04-30 16:05:03",
        const val TIME_TO_RESOLVE_HAS_EXPIRED = "82"
        // Дата решения просрочена(bool) - "82": 0,
        const val DEPARTMENT = "1002"
        // Подразделение - "1002": "Департамент экономического развития",
        const val LOCATION = "83"
        // Местоположение - "83": "Департамент экономического развития",
        const val REAUEST_INITIATOR = "4"
        // Инициатор запроса - "4": 3292,
        const val EMAIL = "34"
        // Email для сопровождения - "34": ""
        const val CATEGORY = "7"
        // Категория - "7": "Сопровождение технических средств",
        const val SPECIALISTS_GROUP = "8"
        // Группа специалистов - "8": "Сопровождение технических средств",
        const val SPECIALIST = "5"
        // Специалист - "5": 5225,
        const val DESCRIPTION_TICKET = "26"
        // Описание(TicketTask) - "26": null,
        const val SATISFACTION = "62"
        // Удовлетворенность - "62": null,
        const val REQUEST_SOURCE = "9"
    }
}