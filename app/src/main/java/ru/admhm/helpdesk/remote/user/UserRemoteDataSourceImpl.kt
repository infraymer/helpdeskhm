package ru.admhm.helpdesk.remote.user

import io.reactivex.Single
import ru.admhm.helpdesk.data.user.source.UserRemoteDataSource
import ru.admhm.helpdesk.domain.user.model.UserModel
import ru.admhm.helpdesk.remote.user.mapper.UserResponseMapper

class UserRemoteDataSourceImpl(
    private val userService: UserService,
    private val userResponseMapper: UserResponseMapper
) : UserRemoteDataSource {

    override fun getUser(userId: Long): Single<UserModel> =
        userService.getUser(userId)
            .map { userResponseMapper.mapToUser(it) }
}