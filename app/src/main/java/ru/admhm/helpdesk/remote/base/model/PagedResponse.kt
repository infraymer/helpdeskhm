package ru.admhm.helpdesk.remote.base.model

interface PagedResponse {
    val totalCount: Int
    val count: Int
    val contentRange: String
}