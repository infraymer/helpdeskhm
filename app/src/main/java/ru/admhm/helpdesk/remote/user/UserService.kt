package ru.admhm.helpdesk.remote.user

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import ru.admhm.helpdesk.remote.user.model.UserResponse

interface UserService {

    @GET("user/{user}")
    fun getUser(@Path("user") userId: Long): Single<UserResponse>
}