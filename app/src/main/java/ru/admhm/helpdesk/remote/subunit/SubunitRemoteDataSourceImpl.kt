package ru.admhm.helpdesk.remote.subunit

import io.reactivex.Single
import ru.admhm.helpdesk.data.subunit.source.SubunitRemoteDataSource
import ru.admhm.helpdesk.domain.subunit.model.SubunitModel
import ru.admhm.helpdesk.remote.subunit.service.SubunitService

class SubunitRemoteDataSourceImpl(
    private val subunitService: SubunitService
) : SubunitRemoteDataSource {

    override fun getSubunitByTask(taskId: Long): Single<SubunitModel> =
        subunitService.getSubunitByTask(taskId)
}