package ru.admhm.helpdesk.remote.task.model

import com.google.gson.annotations.SerializedName

data class TaskResponse(
    @SerializedName("id")
    val id: Long,

    @SerializedName("name")
    val name: String,

    @SerializedName("content")
    val content: String,

    @SerializedName("priority")
    val priorityId: Int,

    @SerializedName("status")
    val statusId: Int,

    @SerializedName("users_id_recipient")
    val initiatorUserId: Long,

    @SerializedName("subunits_id")
    val subunitsId: Long,

    @SerializedName("solvedate")
    val solveDate: String,

    @SerializedName("closedate")
    val closeDate: String,

    @SerializedName("date_creation")
    val creationDate: String
)