package ru.admhm.helpdesk.remote.task_list.mapper

import ru.admhm.helpdesk.domain.task.model.PriorityModel
import ru.admhm.helpdesk.domain.task.model.StatusModel
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.remote.base.serialization.UTCDateAdapter
import ru.admhm.helpdesk.remote.task.model.TaskListItemResponse
import java.util.*

class TaskListMapper {

    private val utcDateAdapter = UTCDateAdapter()

    fun mapToTaskListItemModel(taskListItemResponse: TaskListItemResponse): TaskListItemModel =
        TaskListItemModel(
            taskListItemResponse.id,
            taskListItemResponse.name,
            PriorityModel(taskListItemResponse.priorityId),
            utcDateAdapter.stringToDate(taskListItemResponse.timeOpen) ?: Date(),
            taskListItemResponse.specialistId,
            StatusModel(taskListItemResponse.statusId)
        )
}