package ru.admhm.helpdesk.remote.base.interceptor

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import ru.admhm.helpdesk.cache.base.storage.AuthStorage
import ru.admhm.helpdesk.remote.auth.service.AuthService
import ru.admhm.helpdesk.view.auth.navigation.screen.AuthScreen
import ru.terrakok.cicerone.Router

class AuthInterceptor(
    private val authStorage: AuthStorage,
    private val authService: AuthService,
    private val router: Router
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = authStorage.token
        val authData = authStorage.authData

        val request = chain.request()
            .newBuilder()
            .apply {
                if (token.isNotEmpty())
                    addHeader("Session-Token", token)
                if (authData.isNotEmpty())
                    addHeader("Authorization", "Basic $authData")
            }
            .build()

        var response = chain.proceed(request)

        if (response.code == 401 && authData.isNotEmpty()) {
            response = auth(chain, request)
        }

        return response
    }

    private fun auth(chain: Interceptor.Chain, req: Request): Response {
        var request = req
        try {
            val authResponse = authService.refreshSession("Basic ${authStorage.authData}").execute()
            if (authResponse.code() != 200) throw Throwable("Unauthorized")
            val data = authResponse.body()
            val authToken = data?.token!!
            authStorage.token = authToken
            request = request.newBuilder()
                .removeHeader("Session-Token")
                .addHeader("Session-Token", authToken)
                .build()
            return chain.proceed(request)
        } catch (e: Exception) {
            e.printStackTrace()
            router.newRootScreen(AuthScreen)
            return chain.proceed(request)
        }
    }
}