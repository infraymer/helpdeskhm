package ru.admhm.helpdesk.remote.auth.model

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @SerializedName("session_token")
    val token: String
)