package ru.admhm.helpdesk.remote.role

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.admhm.helpdesk.remote.role.model.ActiveRoleResponse
import ru.admhm.helpdesk.remote.role.model.MyRolesResponse

interface RoleService {

    @GET("getMyProfiles")
    fun getMyRoles(): Single<MyRolesResponse>

    @GET("getActiveProfile")
    fun getActiveRole(): Single<ActiveRoleResponse>

    @GET("changeActiveProfile")
    fun setActiveRole(@Query("profiles_id") roleId: Int): Completable
}