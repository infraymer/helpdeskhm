package ru.admhm.helpdesk.remote.task

import io.reactivex.Single
import ru.admhm.helpdesk.data.task.TaskRemoteDataSource
import ru.admhm.helpdesk.domain.task.model.TaskModel
import ru.admhm.helpdesk.domain.task.model.TaskUserModel
import ru.admhm.helpdesk.remote.task.mapper.TaskResponseMapper
import ru.admhm.helpdesk.remote.task.service.TaskService

class TaskRemoteDataSourceImpl(
    private val taskService: TaskService,
    private val taskResponseMapper: TaskResponseMapper
) : TaskRemoteDataSource {

    override fun getTask(taskId: Long): Single<TaskModel> =
        taskService.getTask(taskId)
            .map { taskResponseMapper.mapToTaskModel(it) }

    override fun getTaskUsers(taskId: Long): Single<List<TaskUserModel>> =
        taskService.getTaskUsers(taskId)
}