package ru.admhm.helpdesk.remote.task.service

import io.reactivex.Single
import retrofit2.http.*
import ru.admhm.helpdesk.domain.task.model.TaskUserModel
import ru.admhm.helpdesk.remote.base.contract.FieldResponseContract
import ru.admhm.helpdesk.remote.base.model.ListResponse
import ru.admhm.helpdesk.remote.task.model.CreateTicketRequest
import ru.admhm.helpdesk.remote.task.model.TaskListItemResponse
import ru.admhm.helpdesk.remote.task.model.TaskResponse

interface TaskService {

    @GET("search/ticket?sort=${FieldResponseContract.TIME_OPEN}&order=DESC")
    fun getTasks(@QueryMap map: Map<String, String>): Single<ListResponse<TaskListItemResponse>>

    @POST("Ticket")
    fun createTask(@Body request: CreateTicketRequest)

    @GET("Ticket/{task}")
    fun getTask(@Path("task") taskId: Long): Single<TaskResponse>

    @GET("Ticket/{task}/ticket_user")
    fun getTaskUsers(@Path("task") taskId: Long): Single<List<TaskUserModel>>
}