package ru.admhm.helpdesk.remote.subunit.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import ru.admhm.helpdesk.domain.subunit.model.SubunitModel

interface SubunitService {

    @GET("ticket/{task}/subunit")
    fun getSubunitByTask(@Path("task") taskId: Long): Single<SubunitModel>
}