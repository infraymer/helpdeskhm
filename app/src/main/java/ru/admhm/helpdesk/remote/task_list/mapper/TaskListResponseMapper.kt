package ru.admhm.helpdesk.remote.task_list.mapper

import ru.admhm.helpdesk.domain.base.model.PagedList
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.remote.base.mapper.toPagedList
import ru.admhm.helpdesk.remote.base.model.ListResponse
import ru.admhm.helpdesk.remote.task.model.TaskListItemResponse

class TaskListResponseMapper(
    private val taskListMapper: TaskListMapper
) {

    fun mapToTaskList(list: ListResponse<TaskListItemResponse>): PagedList<TaskListItemModel> =
        list.toPagedList { it.items.map(taskListMapper::mapToTaskListItemModel) }

}