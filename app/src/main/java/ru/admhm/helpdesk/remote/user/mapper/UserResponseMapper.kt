package ru.admhm.helpdesk.remote.user.mapper

import ru.admhm.helpdesk.domain.user.model.UserModel
import ru.admhm.helpdesk.remote.user.model.UserResponse

class UserResponseMapper {

    fun mapToUser(userResponse: UserResponse): UserModel =
        UserModel(
            userResponse.id,
            userResponse.name,
            userResponse.lastName,
            userResponse.firstName
        )
}