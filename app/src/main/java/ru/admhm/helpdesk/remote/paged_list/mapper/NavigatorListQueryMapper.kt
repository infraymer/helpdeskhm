package ru.admhm.helpdesk.remote.paged_list.mapper

import ru.admhm.helpdesk.domain.paged_list.model.NavigatorListQuery
import ru.admhm.helpdesk.remote.base.serialization.UTCDateAdapter

class NavigatorListQueryMapper {
    companion object {
        private const val KEY_RANGE = "range"

    }

    private val dateAdapter = UTCDateAdapter()

    fun mapToQueryMap(query: NavigatorListQuery): Map<String, String> =
        hashMapOf<String, String>()
            .apply {
                val startElement = query.pageSize * query.page - query.pageSize
                val endElement = query.pageSize * query.page - 1
                this[KEY_RANGE] = "$startElement-$endElement"

                query.authorId?.let {
                }
            }
}