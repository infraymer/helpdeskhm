package ru.admhm.helpdesk.remote.base.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import ru.admhm.helpdesk.base.config.Config

class AppTokenInterceptor(
    private val config: Config
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("App-Token", config.serverAppToken)
            .build()

        return chain.proceed(request)
    }
}