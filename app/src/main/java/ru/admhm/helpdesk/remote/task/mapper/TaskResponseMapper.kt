package ru.admhm.helpdesk.remote.task.mapper

import ru.admhm.helpdesk.domain.task.model.PriorityModel
import ru.admhm.helpdesk.domain.task.model.StatusModel
import ru.admhm.helpdesk.domain.task.model.TaskModel
import ru.admhm.helpdesk.remote.base.serialization.UTCDateAdapter
import ru.admhm.helpdesk.remote.task.model.TaskResponse
import java.util.*

class TaskResponseMapper {

    private val utcDateAdapter = UTCDateAdapter()

    fun mapToTaskModel(taskResponse: TaskResponse): TaskModel =
        TaskModel(
            taskResponse.id,
            taskResponse.name,
            taskResponse.content,
            PriorityModel(taskResponse.priorityId),
            StatusModel(taskResponse.statusId),
            taskResponse.initiatorUserId,
            taskResponse.subunitsId,
            utcDateAdapter.stringToDate(taskResponse.solveDate) ?: Date(),
            utcDateAdapter.stringToDate(taskResponse.closeDate) ?: Date(),
            utcDateAdapter.stringToDate(taskResponse.creationDate) ?: Date()
        )
}