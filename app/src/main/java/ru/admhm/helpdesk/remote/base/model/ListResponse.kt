package ru.admhm.helpdesk.remote.base.model

import com.google.gson.annotations.SerializedName

class ListResponse<T>(
    @SerializedName("totalcount")
    override val totalCount: Int,
    @SerializedName("count")
    override val count: Int,
    @SerializedName("content-range")
    override val contentRange: String,

    @SerializedName("data")
    val items: List<T>
) : PagedResponse