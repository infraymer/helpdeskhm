package ru.admhm.helpdesk.remote.base.model

data class CriteriaRequest(
    val field: String,
    val value: String,
    val searchType: String = EQUALS,
    val link: String = AND
) {

    companion object {
        const val AND = "AND"
        const val AND_NOT = "AND_NOT"

        const val EQUALS = "equals"
    }
}