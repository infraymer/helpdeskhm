package ru.admhm.helpdesk.remote.user.model

import com.google.gson.annotations.SerializedName

class UserResponse(
    @SerializedName("id")
    val id: Long,

    @SerializedName("name")
    val name: String,

    @SerializedName("realname")
    val lastName: String,

    @SerializedName("firstname")
    val firstName: String
)