package ru.admhm.helpdesk.remote.task_list

import io.reactivex.Single
import ru.admhm.helpdesk.data.task_list.source.TaskListRemoteDataSource
import ru.admhm.helpdesk.domain.base.model.PagedList
import ru.admhm.helpdesk.domain.paged_list.model.NavigatorListQuery
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.remote.paged_list.mapper.NavigatorListQueryMapper
import ru.admhm.helpdesk.remote.task.service.TaskService
import ru.admhm.helpdesk.remote.task_list.mapper.TaskListResponseMapper

class TaskListRemoteDataSourceImpl(
    private val taskService: TaskService,
    private val queryMapper: NavigatorListQueryMapper,
    private val taskListResponseMapper: TaskListResponseMapper
) : TaskListRemoteDataSource {

    override fun getTaskList(query: NavigatorListQuery): Single<PagedList<TaskListItemModel>> =
        taskService.getTasks(queryMapper.mapToQueryMap(query))
            .map { taskListResponseMapper.mapToTaskList(it) }
}