package ru.admhm.helpdesk.remote.auth.service

import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import ru.admhm.helpdesk.remote.auth.model.AuthResponse

interface AuthService {

    @GET("initSession")
    fun initSession(@Header("Authorization") auth: String): Single<AuthResponse>

    @GET("initSession")
    fun refreshSession(@Header("Authorization") auth: String): Call<AuthResponse>
}