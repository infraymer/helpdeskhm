package ru.admhm.helpdesk.remote.auth

import android.util.Base64
import io.reactivex.Single
import ru.admhm.helpdesk.data.auth.source.AuthRemoteDataSource
import ru.admhm.helpdesk.domain.auth.model.SignInModel
import ru.admhm.helpdesk.remote.auth.service.AuthService

class AuthRemoteDataSourceImpl(
    private val authService: AuthService
) : AuthRemoteDataSource {

    override fun signIn(signIn: SignInModel): Single<String> {
        val login = signIn.login
        val pass = signIn.password
        val base64 = Base64.encodeToString("$login:$pass".toByteArray(), Base64.NO_WRAP)
        return authService.initSession("Basic $base64")
            .map { it.token }
    }
}