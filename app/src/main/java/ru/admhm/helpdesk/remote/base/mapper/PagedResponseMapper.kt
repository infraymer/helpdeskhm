package ru.admhm.helpdesk.remote.base.mapper

import ru.admhm.helpdesk.domain.base.model.PagedList
import ru.admhm.helpdesk.remote.base.model.PagedResponse


inline fun <E, R : PagedResponse> R.toPagedList(selector: (R) -> List<E>): PagedList<E> {
    val arr = contentRange.split("-", "/")
    val start = arr[0].toInt()
    val end = arr[1].toInt()
    val page = start / count + 1

    return PagedList(
        selector(this),
        page = page,
        hasNext = end < totalCount - 1,
        hasPrev = page > 1,
        total = totalCount
    )
}