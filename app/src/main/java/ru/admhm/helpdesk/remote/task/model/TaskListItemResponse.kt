package ru.admhm.helpdesk.remote.task.model

import com.google.gson.annotations.SerializedName
import ru.admhm.helpdesk.remote.base.contract.FieldResponseContract

class TaskListItemResponse(
    @SerializedName(FieldResponseContract.ID)
    val id: Long,

    @SerializedName(FieldResponseContract.NAME)
    val name: String,

    @SerializedName(FieldResponseContract.PRIORITY)
    val priorityId: Int,

    @SerializedName(FieldResponseContract.TIME_OPEN)
    val timeOpen: String,

    @SerializedName(FieldResponseContract.SPECIALIST)
    val specialistId: Long,

    @SerializedName(FieldResponseContract.STATUS)
    val statusId: Int
)