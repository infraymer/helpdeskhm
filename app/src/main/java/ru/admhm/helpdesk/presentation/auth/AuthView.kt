package ru.admhm.helpdesk.presentation.auth

import ru.admhm.helpdesk.presentation.base.MvpView

interface AuthView : MvpView {

    sealed class State {
        object Idle : State()
        object Loading : State()
        object Error : State()
        object Complete : State()
    }

    fun setState(state: State)
}