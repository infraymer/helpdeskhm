package ru.admhm.helpdesk.presentation.auth

import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.admhm.helpdesk.base.rx.Schedulers
import ru.admhm.helpdesk.domain.auth.interactor.AuthInteractor
import ru.admhm.helpdesk.domain.auth.model.SignInModel
import ru.admhm.helpdesk.presentation.base.MvpPresenter
import ru.admhm.helpdesk.view.task_list.navigation.screen.TaskListScreen
import ru.terrakok.cicerone.Router

class AuthPresenter(
    private val authInteractor: AuthInteractor,
    private val router: Router,
    private val schedulers: Schedulers
) : MvpPresenter<AuthView>() {

    private var state: AuthView.State =
        AuthView.State.Idle
        set(value) {
            field = value
            view?.setState(value)
        }

    fun onSignInClicked(signInModel: SignInModel) {
        compositeDisposable += authInteractor
            .signIn(signInModel)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onComplete = {
                    router.navigateTo(TaskListScreen)
                },
                onError = {
                    state = AuthView.State.Error
                }
            )
    }
}