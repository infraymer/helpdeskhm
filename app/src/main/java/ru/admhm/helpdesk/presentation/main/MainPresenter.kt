package ru.admhm.helpdesk.presentation.main

import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.admhm.helpdesk.base.rx.Schedulers
import ru.admhm.helpdesk.domain.auth.interactor.AuthInteractor
import ru.admhm.helpdesk.presentation.base.MvpPresenter
import ru.admhm.helpdesk.presentation.base.MvpView
import ru.admhm.helpdesk.view.auth.navigation.screen.AuthScreen
import ru.admhm.helpdesk.view.task_detail.navigation.screen.TaskDetailScreen
import ru.admhm.helpdesk.view.task_list.navigation.screen.TaskListScreen
import ru.terrakok.cicerone.Router

class MainPresenter(
    private val authInteractor: AuthInteractor,
    private val router: Router,
    private val schedulers: Schedulers
) : MvpPresenter<MainView>() {

    init {
        compositeDisposable += authInteractor
            .isAuthorized()
            .subscribeBy(
                onComplete = { router.newRootScreen(TaskListScreen) },
                onError = { router.newRootScreen(AuthScreen) }
            )
    }
}