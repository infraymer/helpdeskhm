package ru.admhm.helpdesk.presentation.task

import ru.admhm.helpdesk.domain.task.model.TaskDetailModel
import ru.admhm.helpdesk.presentation.base.MvpView

interface TaskDetailView : MvpView {

    sealed class State {
        object Idle : State()
        object Loading : State()
        object Error : State()
        data class Remote(val task: TaskDetailModel) : State()
    }

    fun setState(state: State)
}