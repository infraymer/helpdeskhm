package ru.admhm.helpdesk.presentation.task_list

import ru.admhm.helpdesk.domain.base.model.PagedList
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.presentation.base.MvpView

interface TaskListView : MvpView {

    sealed class State {
        object Idle : State()
        object Loading : State()
        object Error : State()

        data class Remote(val list: PagedList<TaskListItemModel>) : State()
        data class RemoteLoading(val list: PagedList<TaskListItemModel>) : State()
    }

    fun setState(state: State)
}