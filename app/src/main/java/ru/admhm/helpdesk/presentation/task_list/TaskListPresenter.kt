package ru.admhm.helpdesk.presentation.task_list

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.admhm.helpdesk.base.rx.Schedulers
import ru.admhm.helpdesk.domain.paged_list.model.NavigatorListQuery
import ru.admhm.helpdesk.domain.task_list.interactor.TaskListInteractor
import ru.admhm.helpdesk.presentation.base.MvpPresenter
import ru.terrakok.cicerone.Router

class TaskListPresenter(
    private val taskListInteractor: TaskListInteractor,
    private val router: Router,
    private val schedulers: Schedulers
) : MvpPresenter<TaskListView>() {

    private var state: TaskListView.State =
        TaskListView.State.Idle
        set(value) {
            field = value
            view?.setState(value)
        }

    private val paginationDisposable = CompositeDisposable()

    init {
        compositeDisposable += paginationDisposable

        forceUpdateTasks()
    }

    override fun attachView(view: TaskListView) {
        super.attachView(view)
        view.setState(state)
    }

    fun forceUpdateTasks() {
        paginationDisposable.clear()

        val oldState = state
        state = TaskListView.State.Loading
        paginationDisposable += taskListInteractor
            .getTaskListItems(NavigatorListQuery())
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    state = TaskListView.State.Remote(it)
                },
                onError = {
                    state = oldState
                }
            )
    }

    fun fetchNextPage() {
        val oldState = state

        val oldList = (oldState as? TaskListView.State.Remote)
            ?.list
            ?.takeIf { it.hasNext }
            ?: return

        val nextPage = oldList.page + 1

        state = TaskListView.State.RemoteLoading(oldList)

        paginationDisposable += taskListInteractor
            .getTaskListItems(NavigatorListQuery(page = nextPage))
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    state = TaskListView.State.Remote(it)
                },
                onError = {
                    state = oldState
                }
            )
    }
}