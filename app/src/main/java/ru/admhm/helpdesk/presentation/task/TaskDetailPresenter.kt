package ru.admhm.helpdesk.presentation.task

import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.admhm.helpdesk.base.rx.Schedulers
import ru.admhm.helpdesk.domain.task.interactor.TaskInteractor
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.presentation.base.MvpPresenter
import ru.terrakok.cicerone.Router

class TaskDetailPresenter(
    private val taskInteractor: TaskInteractor,
    private val router: Router,
    private val schedulers: Schedulers
) : MvpPresenter<TaskDetailView>() {

    private var state: TaskDetailView.State =
        TaskDetailView.State.Idle
        set(value) {
            field = value
            view?.setState(state)
        }

    private var taskId: Long = 0L

    fun onTask(task: TaskListItemModel) {
        taskId = task.id
        fetchTaskDetail()
    }

    fun fetchTaskDetail() {
        state = TaskDetailView.State.Loading
        compositeDisposable += taskInteractor
            .getTaskDetail(taskId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    state = TaskDetailView.State.Remote(it)
                },
                onError = {
                    state = TaskDetailView.State.Error
                }
            )
    }
}