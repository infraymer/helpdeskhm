package ru.admhm.helpdesk.data.task_list.repository

import io.reactivex.Single
import ru.admhm.helpdesk.data.task_list.source.TaskListRemoteDataSource
import ru.admhm.helpdesk.domain.base.model.PagedList
import ru.admhm.helpdesk.domain.paged_list.model.NavigatorListQuery
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.domain.task_list.repository.TaskListRepository

class TaskListRepositoryImpl(
    private val taskListRemoteDataSource: TaskListRemoteDataSource
) : TaskListRepository {

    override fun getTaskListItems(query: NavigatorListQuery): Single<PagedList<TaskListItemModel>> =
        taskListRemoteDataSource.getTaskList(query)
}