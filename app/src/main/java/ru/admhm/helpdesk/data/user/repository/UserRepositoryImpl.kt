package ru.admhm.helpdesk.data.user.repository

import io.reactivex.Single
import ru.admhm.helpdesk.data.user.source.UserRemoteDataSource
import ru.admhm.helpdesk.domain.user.model.UserModel
import ru.admhm.helpdesk.domain.user.repository.UserRepository

class UserRepositoryImpl(
    private val userRemoteDataSource: UserRemoteDataSource
) : UserRepository {

    override fun getUser(userId: Long): Single<UserModel> =
        userRemoteDataSource.getUser(userId)
}