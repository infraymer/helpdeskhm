package ru.admhm.helpdesk.data.auth.source

import io.reactivex.Single
import ru.admhm.helpdesk.domain.auth.model.SignInModel

interface AuthRemoteDataSource {

    fun signIn(signIn: SignInModel): Single<String>
}