package ru.admhm.helpdesk.data.task

import io.reactivex.Single
import ru.admhm.helpdesk.domain.task.model.TaskModel
import ru.admhm.helpdesk.domain.task.model.TaskUserModel
import ru.admhm.helpdesk.domain.task.repository.TaskRepository

class TaskRepositoryImpl(
    private val taskRemoteDataSource: TaskRemoteDataSource
) : TaskRepository {

    override fun getTask(taskId: Long): Single<TaskModel> =
        taskRemoteDataSource.getTask(taskId)

    override fun getTaskUsers(taskId: Long): Single<List<TaskUserModel>> =
        taskRemoteDataSource.getTaskUsers(taskId)
}