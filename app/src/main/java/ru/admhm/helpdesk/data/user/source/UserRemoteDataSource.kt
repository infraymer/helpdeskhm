package ru.admhm.helpdesk.data.user.source

import io.reactivex.Single
import ru.admhm.helpdesk.domain.user.model.UserModel

interface UserRemoteDataSource {

    fun getUser(userId: Long): Single<UserModel>
}