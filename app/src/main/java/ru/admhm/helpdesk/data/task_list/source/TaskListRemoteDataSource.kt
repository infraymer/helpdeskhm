package ru.admhm.helpdesk.data.task_list.source

import io.reactivex.Single
import ru.admhm.helpdesk.domain.base.model.PagedList
import ru.admhm.helpdesk.domain.paged_list.model.NavigatorListQuery
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel

interface TaskListRemoteDataSource {

    fun getTaskList(query: NavigatorListQuery): Single<PagedList<TaskListItemModel>>
}