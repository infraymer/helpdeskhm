package ru.admhm.helpdesk.data.task

import io.reactivex.Single
import ru.admhm.helpdesk.domain.task.model.TaskModel
import ru.admhm.helpdesk.domain.task.model.TaskUserModel

interface TaskRemoteDataSource {

    fun getTask(taskId: Long): Single<TaskModel>

    fun getTaskUsers(taskId: Long): Single<List<TaskUserModel>>
}