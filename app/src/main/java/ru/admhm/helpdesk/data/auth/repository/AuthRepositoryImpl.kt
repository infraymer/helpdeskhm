package ru.admhm.helpdesk.data.auth.repository

import io.reactivex.Completable
import ru.admhm.helpdesk.data.auth.source.AuthCacheDataSource
import ru.admhm.helpdesk.data.auth.source.AuthRemoteDataSource
import ru.admhm.helpdesk.domain.auth.model.SignInModel
import ru.admhm.helpdesk.domain.auth.repository.AuthRepository
import ru.admhm.helpdesk.domain.base.util.doCompletableOnSuccess

class AuthRepositoryImpl(
    private val authRemoteDataSource: AuthRemoteDataSource,
    private val authCacheDataSource: AuthCacheDataSource
) : AuthRepository {

    override fun signIn(signIn: SignInModel): Completable =
        authRemoteDataSource
            .signIn(signIn)
            .doCompletableOnSuccess { authCacheDataSource.setAuthData(signIn) }
            .doCompletableOnSuccess(authCacheDataSource::setSessionToken)
            .ignoreElement()

    override fun isAuthorized(): Completable =
        Completable.mergeArray(
            getSessionToken(),
            authCacheDataSource.authDataIsExist()
        )

    private fun getSessionToken(): Completable =
        authCacheDataSource.getSessionToken()
            .flatMapCompletable {
                Completable
                    .fromAction { if (it.isEmpty()) throw Throwable("Session token is empty") }
            }

}