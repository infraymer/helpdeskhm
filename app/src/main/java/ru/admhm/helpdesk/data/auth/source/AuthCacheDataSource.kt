package ru.admhm.helpdesk.data.auth.source

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import ru.admhm.helpdesk.domain.auth.model.SignInModel

interface AuthCacheDataSource {

    fun setSessionToken(token: String): Completable

    fun getSessionToken(): Single<String>

    fun setAuthData(signInModel: SignInModel): Completable

    fun authDataIsExist(): Completable
}