package ru.admhm.helpdesk.data.subunit.repository

import io.reactivex.Single
import ru.admhm.helpdesk.data.subunit.source.SubunitRemoteDataSource
import ru.admhm.helpdesk.domain.subunit.model.SubunitModel
import ru.admhm.helpdesk.domain.subunit.repository.SubunitRepository

class SubunitRepositoryImpl(
    private val subunitRemoteDataSource: SubunitRemoteDataSource
) : SubunitRepository {

    override fun getSubunitByTask(taskId: Long): Single<SubunitModel> =
        subunitRemoteDataSource.getSubunitByTask(taskId)
}