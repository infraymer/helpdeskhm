package ru.admhm.helpdesk.data.subunit.source

import io.reactivex.Single
import ru.admhm.helpdesk.domain.subunit.model.SubunitModel

interface SubunitRemoteDataSource {

    fun getSubunitByTask(taskId: Long): Single<SubunitModel>
}