package ru.admhm.helpdesk.view.main.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.admhm.helpdesk.R
import ru.admhm.helpdesk.cache.base.AppStorage
import ru.admhm.helpdesk.presentation.base.MvpView
import ru.admhm.helpdesk.presentation.main.MainPresenter
import ru.admhm.helpdesk.presentation.main.MainView
import ru.admhm.helpdesk.view.base.ui.listener.BackButtonListener
import ru.admhm.helpdesk.view.base.ui.listener.BackNavigable
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class MainActivity : AppCompatActivity(), BackNavigable, MainView {

    private val router: Router by inject()
    private val navigatorHolder: NavigatorHolder by inject()

    private val navigator = SupportAppNavigator(this, supportFragmentManager, R.id.container)

    private val presenter: MainPresenter by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*if (savedInstanceState == null) {
            router.newRootScreen(AuthScreen)
        }*/
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onStop() {
        presenter.detachView(this)
        super.onStop()
    }

    override fun onBackPressed() {
        (supportFragmentManager.findFragmentById(R.id.container) as? BackButtonListener)
            ?.onBackPressed()
            ?: router.exit()
    }

    override fun onNavigateBack() {
        router.exit()
    }
}
