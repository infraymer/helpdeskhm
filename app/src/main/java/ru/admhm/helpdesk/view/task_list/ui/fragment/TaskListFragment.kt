package ru.admhm.helpdesk.view.task_list.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_task_list.*
import kotlinx.android.synthetic.main.layout_simple_appbar.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.admhm.helpdesk.R
import ru.admhm.helpdesk.presentation.task_list.TaskListPresenter
import ru.admhm.helpdesk.presentation.task_list.TaskListView
import ru.admhm.helpdesk.view.base.ui.fragment.MvpFragment
import ru.admhm.helpdesk.view.task_detail.navigation.screen.TaskDetailScreen
import ru.admhm.helpdesk.view.task_list.model.TaskItem
import ru.admhm.helpdesk.view.task_list.ui.adapter.delegate.TaskAdapterDelegate
import ru.admhm.helpdesk.view.util.extension.doOnScrolledToEnd
import ru.nobird.android.ui.adapters.DefaultDelegateAdapter
import ru.terrakok.cicerone.Router

class TaskListFragment : MvpFragment<TaskListPresenter, TaskListView>(), TaskListView {
    companion object {
        fun newInstance(): Fragment =
            TaskListFragment()
    }

    override val layoutRes: Int = R.layout.fragment_task_list
    override val presenter: TaskListPresenter by viewModel()
    override val mvpView: TaskListView = this

    private val router: Router by inject()

    private val tasksAdapter = DefaultDelegateAdapter<TaskItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tasksAdapter += TaskAdapterDelegate { router.navigateTo(TaskDetailScreen(it.task)) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appbarTitle.text = "Заявки"

        with(tasksRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = tasksAdapter
            doOnScrolledToEnd { presenter.fetchNextPage() }
        }

        refresh.setOnRefreshListener { presenter.forceUpdateTasks() }
    }

    override fun setState(state: TaskListView.State) {
        when (state) {
            is TaskListView.State.Remote -> {
                tasksAdapter.items = state.list.map { TaskItem.Main(it) }
            }
        }
        refresh.isRefreshing = state is TaskListView.State.Loading
    }
}