package ru.admhm.helpdesk.view.auth.navigation.screen

import androidx.fragment.app.Fragment
import ru.admhm.helpdesk.view.auth.ui.fragment.AuthFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object AuthScreen : SupportAppScreen() {

    override fun getFragment(): Fragment =
        AuthFragment()
}