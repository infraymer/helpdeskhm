package ru.admhm.helpdesk.view.injection.task_list

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.admhm.helpdesk.domain.task_list.interactor.TaskListInteractor
import ru.admhm.helpdesk.presentation.task_list.TaskListPresenter

val taskListModule = module {
    factory { TaskListInteractor(get()) }
    viewModel { TaskListPresenter(get(), get(), get()) }
}