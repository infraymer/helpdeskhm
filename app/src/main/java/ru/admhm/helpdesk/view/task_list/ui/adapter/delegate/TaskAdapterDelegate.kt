package ru.admhm.helpdesk.view.task_list.ui.adapter.delegate

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_task.*
import kotlinx.android.synthetic.main.layout_task_time.*
import ru.admhm.helpdesk.R
import ru.admhm.helpdesk.view.task_list.model.TaskItem
import ru.admhm.helpdesk.view.util.extension.asString
import ru.nobird.android.ui.adapterdelegates.AdapterDelegate
import ru.nobird.android.ui.adapterdelegates.DelegateViewHolder
import java.util.*


class TaskAdapterDelegate(
    private val onClicked: (TaskItem.Main) -> Unit = {}
) : AdapterDelegate<TaskItem, DelegateViewHolder<TaskItem>>() {
    override fun isForViewType(position: Int, data: TaskItem): Boolean =
        data is TaskItem.Main

    override fun onCreateViewHolder(parent: ViewGroup): DelegateViewHolder<TaskItem> =
        ViewHolder(createView(parent, R.layout.item_task))

    private inner class ViewHolder(
        override val containerView: View
    ) : LayoutContainer,
        DelegateViewHolder<TaskItem>(containerView) {

        init {
            containerView.setOnClickListener { (itemData as? TaskItem.Main)?.let(onClicked) }
        }

        @SuppressLint("SetTextI18n")
        override fun onBind(data: TaskItem) {
            data as TaskItem.Main

            task_id.text = "ID ${data.task.id}"
            task_priority.text = data.task.priority.name
            task_title.text = data.task.name


            // fixme расчитать время от открытия заявки до текущего момент и отформатировать
            val time = Date().time - data.task.timeOpen.time
            task_time.text = time.asString("HH:mm:ss")

            val priority = data.task.priority.id

            val priorityColor = when {
                priority < 3 -> getColorGreen()
                priority > 3 -> getColorRed()
                else -> getColorYellow()
            }

            task_priority.setTextColor(priorityColor)
            task_priority.setCompoundDrawablesWithIntrinsicBounds(
                getCircleResFromPriority(priority), 0, 0, 0
            )

            ImageViewCompat.setImageTintList(task_time_icon, ColorStateList.valueOf(priorityColor))
        }

        private fun getCircleResFromPriority(priority: Int): Int =
            when {
                priority < 3 -> R.drawable.ic_circle_green
                priority > 3 -> R.drawable.ic_circle_red
                else -> R.drawable.ic_circle_yellow
            }

        private fun getColorRes(@ColorRes color: Int): Int =
            ContextCompat.getColor(context, color)

        private fun getColorRed() = getColorRes(R.color.red)
        private fun getColorYellow() = getColorRes(R.color.yellow)
        private fun getColorGreen() = getColorRes(R.color.green)

    }
}