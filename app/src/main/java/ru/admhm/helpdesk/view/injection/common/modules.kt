package ru.admhm.helpdesk.view.injection.common

import ru.admhm.helpdesk.view.injection.app.appModule
import ru.admhm.helpdesk.view.injection.auth.authDataModule
import ru.admhm.helpdesk.view.injection.auth.authModule
import ru.admhm.helpdesk.view.injection.main.mainModule
import ru.admhm.helpdesk.view.injection.network.networkModule
import ru.admhm.helpdesk.view.injection.subunit.subunitDataModule
import ru.admhm.helpdesk.view.injection.task.taskDataModule
import ru.admhm.helpdesk.view.injection.task.taskModule
import ru.admhm.helpdesk.view.injection.task_list.taskListDataModule
import ru.admhm.helpdesk.view.injection.task_list.taskListModule
import ru.admhm.helpdesk.view.injection.user.userDataModule

val modules = listOf(
    appModule,

    networkModule,

    remoteMappers,

    mainModule,

    authDataModule,
    authModule,

    userDataModule,

    subunitDataModule,

    taskDataModule,
    taskModule,

    taskListDataModule,
    taskListModule
)