package ru.admhm.helpdesk.view.injection.auth

import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import ru.admhm.helpdesk.cache.auth.AuthCacheDataSourceImpl
import ru.admhm.helpdesk.data.auth.repository.AuthRepositoryImpl
import ru.admhm.helpdesk.data.auth.source.AuthCacheDataSource
import ru.admhm.helpdesk.data.auth.source.AuthRemoteDataSource
import ru.admhm.helpdesk.domain.auth.repository.AuthRepository
import ru.admhm.helpdesk.remote.auth.AuthRemoteDataSourceImpl
import ru.admhm.helpdesk.remote.auth.service.AuthService

val authDataModule = module {
    factory<AuthService> { get<Retrofit>(named("auth")).create(AuthService::class.java) }

    factory<AuthRemoteDataSource> { AuthRemoteDataSourceImpl(get()) }
    factory<AuthCacheDataSource> { AuthCacheDataSourceImpl(get()) }
    factory<AuthRepository> { AuthRepositoryImpl(get(), get()) }
}