package ru.admhm.helpdesk.view.base.ui.fragment

import ru.admhm.helpdesk.presentation.base.MvpPresenter
import ru.admhm.helpdesk.presentation.base.MvpView

//abstract class MvpFragment<P : MvpPresenter<V>, V : MvpView> : BaseFragment(), MvpView {
abstract class MvpFragment<P : MvpPresenter<V>, V : MvpView> : BaseFragment() {

    abstract val presenter: P
    abstract val mvpView: V

    override fun onStart() {
        super.onStart()
        presenter.attachView(mvpView)
    }

    override fun onStop() {
        presenter.detachView(mvpView)
        super.onStop()
    }
}