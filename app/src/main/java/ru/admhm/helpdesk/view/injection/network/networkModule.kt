package ru.admhm.helpdesk.view.injection.network

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.admhm.helpdesk.base.config.Config
import ru.admhm.helpdesk.remote.base.interceptor.AppTokenInterceptor
import ru.admhm.helpdesk.remote.base.interceptor.AuthInterceptor
import ru.admhm.helpdesk.remote.base.serialization.UTCDateAdapter
import java.security.cert.X509Certificate
import java.util.*
import javax.net.ssl.*
import javax.security.cert.CertificateException



val networkModule = module {

    factory { AuthInterceptor(get(), get(), get()) }
    factory { AppTokenInterceptor(get()) }

    factory {
        GsonBuilder()
            .registerTypeAdapter(Date::class.java, UTCDateAdapter())
            .create()
            .let(GsonConverterFactory::create)
    }

    val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
        @Throws(CertificateException::class)
        override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}
        @Throws(CertificateException::class)
        override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}
        override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
    })
    val sslContext = SSLContext.getInstance("SSL")
    sslContext.init(null, trustAllCerts, java.security.SecureRandom())
    val sslSocketFactory = sslContext.socketFactory

    factory {
        OkHttpClient.Builder()
            .addInterceptor(get<AppTokenInterceptor>())
            .addInterceptor(get<AuthInterceptor>())
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            .hostnameVerifier(HostnameVerifier { hostname, session -> true })
            .build()
    }

    factory {
        Retrofit.Builder()
            .baseUrl(get<Config>().serverUrl)
            .addConverterFactory(get<GsonConverterFactory>())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(get())
            .build()
    }

    factory(named("auth")) {
        val client = OkHttpClient.Builder()
            .addInterceptor(get<AppTokenInterceptor>())
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            .hostnameVerifier(HostnameVerifier { hostname, session -> true })
            .build()

        Retrofit.Builder()
            .baseUrl(get<Config>().serverUrl)
            .addConverterFactory(get<GsonConverterFactory>())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
    }
}