package ru.admhm.helpdesk.view.injection.task_list

import org.koin.dsl.module
import ru.admhm.helpdesk.data.task_list.repository.TaskListRepositoryImpl
import ru.admhm.helpdesk.data.task_list.source.TaskListRemoteDataSource
import ru.admhm.helpdesk.domain.task_list.repository.TaskListRepository
import ru.admhm.helpdesk.remote.task_list.TaskListRemoteDataSourceImpl
import ru.admhm.helpdesk.remote.task_list.mapper.TaskListMapper
import ru.admhm.helpdesk.remote.task_list.mapper.TaskListResponseMapper

val taskListDataModule = module {
    factory<TaskListRemoteDataSource> { TaskListRemoteDataSourceImpl(get(), get(), get()) }
    factory<TaskListRepository> { TaskListRepositoryImpl(get()) }
}