package ru.admhm.helpdesk.view.injection.auth

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.admhm.helpdesk.domain.auth.interactor.AuthInteractor
import ru.admhm.helpdesk.presentation.auth.AuthPresenter

val authModule = module {
    factory { AuthInteractor(get()) }
    viewModel { AuthPresenter(get(), get(), get()) }
}