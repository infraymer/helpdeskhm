package ru.admhm.helpdesk.view.injection.task

import org.koin.dsl.module
import retrofit2.Retrofit
import ru.admhm.helpdesk.data.task.TaskRemoteDataSource
import ru.admhm.helpdesk.data.task.TaskRepositoryImpl
import ru.admhm.helpdesk.domain.task.repository.TaskRepository
import ru.admhm.helpdesk.remote.task.TaskRemoteDataSourceImpl
import ru.admhm.helpdesk.remote.task.service.TaskService

val taskDataModule = module {
    factory<TaskService> { get<Retrofit>().create(TaskService::class.java) }

    factory<TaskRemoteDataSource> { TaskRemoteDataSourceImpl(get(), get()) }
    factory<TaskRepository> { TaskRepositoryImpl(get()) }
}