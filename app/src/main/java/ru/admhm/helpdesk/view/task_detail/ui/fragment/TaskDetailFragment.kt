package ru.admhm.helpdesk.view.task_detail.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.layout_task_info.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.admhm.helpdesk.R
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.presentation.task.TaskDetailPresenter
import ru.admhm.helpdesk.presentation.task.TaskDetailView
import ru.admhm.helpdesk.remote.base.fragment.argument
import ru.admhm.helpdesk.view.base.ui.fragment.MvpFragment

class TaskDetailFragment : MvpFragment<TaskDetailPresenter, TaskDetailView>(), TaskDetailView {
    companion object {
        fun newInstance(taskListItem: TaskListItemModel): Fragment =
            TaskDetailFragment().apply {
                this.taskListItem = taskListItem
            }
    }

    override val layoutRes: Int = R.layout.fragment_task_detail
    override val presenter: TaskDetailPresenter by viewModel()
    override val mvpView: TaskDetailView = this

    private var taskListItem: TaskListItemModel by argument()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onTask(taskListItem)
    }

    override fun setState(state: TaskDetailView.State) {
        when (state) {
            is TaskDetailView.State.Loading -> {
            }
            is TaskDetailView.State.Remote -> {
                with(state.task.taskModel) {
                    task_priority.text = priority.name
                    task_title.text = name
                    task_content.text = content
                }
            }
        }
    }
}