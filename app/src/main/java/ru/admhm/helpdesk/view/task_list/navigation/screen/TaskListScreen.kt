package ru.admhm.helpdesk.view.task_list.navigation.screen

import androidx.fragment.app.Fragment
import ru.admhm.helpdesk.view.task_list.ui.fragment.TaskListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object TaskListScreen : SupportAppScreen() {

    override fun getFragment(): Fragment =
        TaskListFragment.newInstance()
}