package ru.admhm.helpdesk.view.base.ui.listener

interface BackButtonListener {
    fun onBackPressed(): Boolean
}