package ru.admhm.helpdesk.view.injection.common

import org.koin.dsl.module
import ru.admhm.helpdesk.remote.paged_list.mapper.NavigatorListQueryMapper
import ru.admhm.helpdesk.remote.task.mapper.TaskResponseMapper
import ru.admhm.helpdesk.remote.task_list.mapper.TaskListMapper
import ru.admhm.helpdesk.remote.task_list.mapper.TaskListResponseMapper
import ru.admhm.helpdesk.remote.user.mapper.UserResponseMapper

val remoteMappers = module {
    factory { NavigatorListQueryMapper() }

    // task_list
    factory { TaskListMapper() }
    factory { TaskListResponseMapper(get()) }

    // task
    factory { TaskResponseMapper() }

    // user
    factory { UserResponseMapper() }
}