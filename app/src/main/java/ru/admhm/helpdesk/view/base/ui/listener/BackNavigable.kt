package ru.admhm.helpdesk.view.base.ui.listener

interface BackNavigable {
    /**
     * Called when nested navigation cannot navigate back
     */
    fun onNavigateBack()
}