package ru.admhm.helpdesk.view.task_list.model

import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel

sealed class TaskItem {

    data class Main(val task: TaskListItemModel) : TaskItem()
}