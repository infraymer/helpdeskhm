package ru.admhm.helpdesk.view.injection.app

import org.koin.dsl.module
import ru.admhm.helpdesk.base.config.Config
import ru.admhm.helpdesk.base.config.ConfigImpl
import ru.admhm.helpdesk.base.rx.Schedulers
import ru.admhm.helpdesk.base.rx.SchedulersImpl
import ru.admhm.helpdesk.cache.base.AppStorage
import ru.admhm.helpdesk.cache.base.storage.AuthStorage
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

val appModule = module {

    factory<Schedulers> { SchedulersImpl() }

    val cicerone = Cicerone.create()
    single<Router> { cicerone.router }
    single<NavigatorHolder> { cicerone.navigatorHolder }

    single<Config> { ConfigImpl() }

    factory<AuthStorage> { AppStorage }
}