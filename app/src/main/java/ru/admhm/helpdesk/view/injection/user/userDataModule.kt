package ru.admhm.helpdesk.view.injection.user

import org.koin.dsl.module
import retrofit2.Retrofit
import ru.admhm.helpdesk.data.user.repository.UserRepositoryImpl
import ru.admhm.helpdesk.data.user.source.UserRemoteDataSource
import ru.admhm.helpdesk.domain.user.repository.UserRepository
import ru.admhm.helpdesk.remote.user.UserRemoteDataSourceImpl
import ru.admhm.helpdesk.remote.user.UserService

val userDataModule = module {
    factory<UserService> { get<Retrofit>().create(UserService::class.java) }

    factory<UserRemoteDataSource> { UserRemoteDataSourceImpl(get(), get()) }
    factory<UserRepository> { UserRepositoryImpl(get()) }
}