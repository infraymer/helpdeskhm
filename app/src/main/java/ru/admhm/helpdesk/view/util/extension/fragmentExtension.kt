package ru.admhm.helpdesk.view.util.extension

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

fun Fragment.toast(text: String, long: Boolean = false) = context?.toast(text, long)
fun Fragment.toast(@StringRes resId: Int, long: Boolean = false) = context?.toast(resId, long)