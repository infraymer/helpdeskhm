package ru.admhm.helpdesk.view.auth.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isInvisible
import kotlinx.android.synthetic.main.fragment_auth.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.admhm.helpdesk.R
import ru.admhm.helpdesk.domain.auth.model.SignInModel
import ru.admhm.helpdesk.presentation.auth.AuthPresenter
import ru.admhm.helpdesk.presentation.auth.AuthView
import ru.admhm.helpdesk.view.base.ui.fragment.MvpFragment
import ru.admhm.helpdesk.view.util.extension.toast

class AuthFragment : MvpFragment<AuthPresenter, AuthView>(), AuthView {

    override val layoutRes: Int = R.layout.fragment_auth

    override val presenter: AuthPresenter by viewModel()
    override val mvpView: AuthView = this

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionButton.setOnClickListener {
            presenter.onSignInClicked(
                SignInModel(
                    loginEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            )
        }

        // fixme это временно для тестов
//        loginEditText.setText("Tey")
//        passwordEditText.setText("Oyrv^3&23kj3")
        loginEditText.setText("TeyD")
        passwordEditText.setText("Reb^3gYerQW")
    }

    override fun setState(state: AuthView.State) {
        when (state) {
            is AuthView.State.Complete -> {
                toast("Complete!")
            }
            is AuthView.State.Error -> {
                toast("Error...")
            }
        }
        actionButton.isInvisible = state is AuthView.State.Loading
        progressBar.isInvisible = state !is AuthView.State.Loading
    }
}