package ru.admhm.helpdesk.view.task_detail.navigation.screen

import androidx.fragment.app.Fragment
import ru.admhm.helpdesk.domain.task_list.model.TaskListItemModel
import ru.admhm.helpdesk.view.task_detail.ui.fragment.TaskDetailFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

data class TaskDetailScreen(
    private val taskListItem: TaskListItemModel
) : SupportAppScreen() {

    override fun getFragment(): Fragment =
        TaskDetailFragment.newInstance(taskListItem)
}