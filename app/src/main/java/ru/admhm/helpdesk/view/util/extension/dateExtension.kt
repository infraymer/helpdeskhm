package ru.admhm.helpdesk.view.util.extension

import java.text.SimpleDateFormat
import java.util.*


fun timestamp(): Long = System.currentTimeMillis()

fun Date.asString(format: String): String = SimpleDateFormat(format).format(this)
fun Long.asString(format: String): String = SimpleDateFormat(format).format(Date(this))

fun Date.isSameDay(date: Date): Boolean {
    val cal1: Calendar = Calendar.getInstance()
    cal1.time = this
    val cal2: Calendar = Calendar.getInstance()
    cal2.time = date
    return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
            cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
}

fun Date.isYesterday(date: Date): Boolean {
    val cal1: Calendar = Calendar.getInstance()
    cal1.time = this
    val cal2: Calendar = Calendar.getInstance()
    cal2.time = date
    cal2.add(Calendar.DAY_OF_YEAR, -1)
    return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
            cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
            cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR)
}

fun today(): Long = Calendar.getInstance()
    .timeInMillis

fun tomorrow(): Long = Calendar.getInstance()
    .timeInMillis
    .plusDay(1)

fun Long.daysInMonth() = Calendar.getInstance().apply {
    timeInMillis = this@daysInMonth
}.getActualMaximum(Calendar.DAY_OF_MONTH)

val Long.hour: Int
    get() = Calendar.getInstance().apply {
        timeInMillis = this@hour
    }.get(Calendar.HOUR)

val Long.day: Int
    get() = Calendar.getInstance().apply {
        timeInMillis = this@day
    }.get(Calendar.DAY_OF_MONTH)

val Long.month: Int
    get() = Calendar.getInstance().apply {
        timeInMillis = this@month
    }.get(Calendar.MONTH)

fun Long.plusHour(hour: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusHour
    add(Calendar.HOUR_OF_DAY, hour)
}.timeInMillis

fun Long.minusHour(hour: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusHour
    add(Calendar.HOUR_OF_DAY, hour)
}.timeInMillis

fun Long.plusDay(day: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusDay
    add(Calendar.DAY_OF_MONTH, day)
}.timeInMillis

fun Long.minusDay(day: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusDay
    add(Calendar.DAY_OF_MONTH, day)
}.timeInMillis

fun Long.plusMonth(month: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusMonth
    add(Calendar.MONTH, month)
}.timeInMillis

fun Long.minusMonth(month: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusMonth
    add(Calendar.MONTH, month)
}.timeInMillis

fun Long.plusYear(year: Int = 1) = Calendar.getInstance().apply {
    timeInMillis = this@plusYear
    add(Calendar.YEAR, year)
}.timeInMillis

fun Long.minusYear(year: Int = -1) = Calendar.getInstance().apply {
    timeInMillis = this@minusYear
    add(Calendar.YEAR, year)
}.timeInMillis