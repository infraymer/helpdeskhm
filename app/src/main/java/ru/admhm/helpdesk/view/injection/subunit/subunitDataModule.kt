package ru.admhm.helpdesk.view.injection.subunit

import org.koin.dsl.module
import retrofit2.Retrofit
import ru.admhm.helpdesk.data.subunit.repository.SubunitRepositoryImpl
import ru.admhm.helpdesk.data.subunit.source.SubunitRemoteDataSource
import ru.admhm.helpdesk.domain.subunit.repository.SubunitRepository
import ru.admhm.helpdesk.remote.subunit.SubunitRemoteDataSourceImpl
import ru.admhm.helpdesk.remote.subunit.service.SubunitService

val subunitDataModule = module {
    factory<SubunitService> { get<Retrofit>().create(SubunitService::class.java) }
    factory<SubunitRemoteDataSource> { SubunitRemoteDataSourceImpl(get()) }
    factory<SubunitRepository> { SubunitRepositoryImpl(get()) }
}