package ru.admhm.helpdesk.view.injection.task

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.admhm.helpdesk.domain.task.interactor.TaskInteractor
import ru.admhm.helpdesk.presentation.task.TaskDetailPresenter

val taskModule = module {
    factory { TaskInteractor(get(), get(), get()) }
    viewModel { TaskDetailPresenter(get(), get(), get()) }
}