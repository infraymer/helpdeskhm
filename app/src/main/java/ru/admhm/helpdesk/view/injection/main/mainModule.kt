package ru.admhm.helpdesk.view.injection.main

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.admhm.helpdesk.presentation.main.MainPresenter

val mainModule = module {
    viewModel { MainPresenter(get(), get(), get()) }
}