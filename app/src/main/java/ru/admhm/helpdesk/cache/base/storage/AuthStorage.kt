package ru.admhm.helpdesk.cache.base.storage

interface AuthStorage {

    var token: String
    var authData: String
}