package ru.admhm.helpdesk.cache.base

import com.chibatching.kotpref.KotprefModel
import ru.admhm.helpdesk.cache.base.storage.AuthStorage

object AppStorage : KotprefModel(), AuthStorage {

    override var token: String by stringPref()
    override var authData: String by stringPref()
}