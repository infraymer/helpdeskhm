package ru.admhm.helpdesk.cache.auth

import android.util.Base64
import io.reactivex.Completable
import io.reactivex.Single
import ru.admhm.helpdesk.cache.base.storage.AuthStorage
import ru.admhm.helpdesk.data.auth.source.AuthCacheDataSource
import ru.admhm.helpdesk.domain.auth.model.SignInModel

class AuthCacheDataSourceImpl(
    private val authStorage: AuthStorage
) : AuthCacheDataSource {

    override fun setSessionToken(token: String): Completable =
        Completable.fromAction { authStorage.token = token }

    override fun getSessionToken(): Single<String> =
        Single.fromCallable { authStorage.token }

    override fun setAuthData(signInModel: SignInModel): Completable =
        Completable.fromAction {
            val login = signInModel.login
            val pass = signInModel.password
            val base64 = Base64.encodeToString("$login:$pass".toByteArray(), Base64.NO_WRAP)
            authStorage.authData = base64
        }

    override fun authDataIsExist(): Completable =
        Completable.fromAction {
            if (authStorage.authData.isEmpty())
                throw Throwable("Auth data is not exist")
        }
}