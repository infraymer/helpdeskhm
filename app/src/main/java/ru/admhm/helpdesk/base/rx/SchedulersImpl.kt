package ru.admhm.helpdesk.base.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

class SchedulersImpl : Schedulers {
    override fun ui(): Scheduler = AndroidSchedulers.mainThread()
    override fun computation(): Scheduler = io.reactivex.schedulers.Schedulers.computation()
    override fun newThread(): Scheduler = io.reactivex.schedulers.Schedulers.newThread()
    override fun io(): Scheduler = io.reactivex.schedulers.Schedulers.io()
}