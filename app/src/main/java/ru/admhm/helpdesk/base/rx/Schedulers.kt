package ru.admhm.helpdesk.base.rx

import io.reactivex.Scheduler

interface Schedulers {
    fun ui(): Scheduler
    fun computation(): Scheduler
    fun newThread(): Scheduler
    fun io(): Scheduler
}