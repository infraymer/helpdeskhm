package ru.admhm.helpdesk.base.config

interface Config {
    val serverUrl: String
    val serverAppToken: String
}